const path = require(`path`)

const { browserify } = require(`@suzan_pevensive/browserify-js`)

const assign = require(`./src/assign`)

let both = {

  add: require(`./src/add`),
  any: require(`./src/any`),
  contains: require(`./src/contains`),
  cut: require(`./src/cut`),
  filter: require(`./src/filter`),
  first: require(`./src/first`),
  forEach: require(`./src/forEach`),
  map: require(`./src/map`),
  remove: require(`./src/remove`)

}

let objects = assign({

  assign: require(`./src/assign`),
  check: require(`./src/check`),
  flat: require(`./src/flat`),
  keys: require(`./src/keys`),
  size: require(`./src/size`),
  values: require(`./src/values`)

}, both)

let arrays = assign({

  refill: require(`./src/refill`)

}, both)

let main = {

  toArray: require(`./src/toArray`),
  toObject: require(`./src/toObject`),

  objects,
  arrays,

  browserify: (app) => browserify.publishModule(app, `CollectionsJs`, path.join(__dirname, `src`), `js`)

}

module.exports = assign(main, objects, arrays)

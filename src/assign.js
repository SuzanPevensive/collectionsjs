/**

  @description
    connects [object] and some [entries] and return as new object

  @param {object}       object - first object in connection
  @param {...objects}   entries - next objects in connection
  @return {object}

  @example
    let first = { number: 12  }
    let second = { number: 15 }
    let other = { letter: 'a' }
    let result = assign(first, second, other)
    result == { number: 15, letter: 'a' }
    
*/
module.exports = function(object, ...entries){
  return Object.assign({}, object, ...entries)
}

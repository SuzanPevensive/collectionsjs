/**

  @description
    return [array] with [object] values
object
  @param {object}     object - object to get values
  @return {array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = keys(target)
    result == [1, 5, 12]

*/
module.exports = function(object){
  return Object.values(object)
}

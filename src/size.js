const keys = require(`./keys`)

/**

  @description
    returns number of [object] keys

  @param {object}   object - object to count keys
  @return {int}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = size(target)
    result == 3

*/
module.exports = function(object){
  return keys(object).length
}

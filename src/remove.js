const forEach = require(`./forEach`)
const add = require(`./add`)

//@browserify:getter
const { empty } = require(`@suzan_pevensive/mock-js`)

/**

  @description
    returns a copy of [target] with removed [fields]

  @param {object/array}   target - object or array as source
  @param {array}          fields - fields to removed
  @return {object/array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = remove(target, [`second`])
    result == { first: 1, other: 12 }

*/
module.exports = function(target, fields){
  //@browserify:get(empty)
  let obj = empty(target)
  forEach(target, (key, value) => {
    if(!fields.includes(key)) add(obj, key, value)
  })
  return obj
}

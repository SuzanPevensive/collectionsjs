module.exports = function(object, fields, canBeNull = false){
  for(field of fields){
    if(typeof object[field] == `undefined` || (object[field] == null && !canBeNull)){
      return false
    }
  }
  return true
}

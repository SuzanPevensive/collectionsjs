/**

  @description
    return [array] with [object] keys
object
  @param {object}     object - object to get keys
  @return {array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = keys(target)
    result == [`first`, `second`, `other`]

*/
module.exports = function(object){
  return Object.keys(object)
}

/**

  @description
    invoke [callback] for all fields from [target]

  @param {object/array}   target - object or array to iterate
  @param {function}       callback - function invoked for all fields from [target]
  @return {nothing}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = 1
    forEach(target, (key, value)=>( result *= value ))
    result == 60

*/
module.exports = function(target, callback){
  Object.entries(target).forEach(([key, value]) => { callback(key, value) })
}

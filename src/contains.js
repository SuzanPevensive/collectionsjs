/**

  @description
    checks if [target] contains [values] on specific [fields]
      if [canBeNull] [values] can equals null

  @param {object/array}   target - object or array to check
  @param {array}          fields - fields witch must have [target]
  @param {boolean}        canBeNull - additional condition allowing for interpreting
                                      null as defined [rtarget] value
  @return {boolean}

  @example "target has fields"
    let target = { first: 1, second: 5, other: 12 }
    let result = contains(target, [`first`, `other`])
    result == true

  @example "target hasn't fields"
    let target = { first: 1, second: 5 }
    let result = contains(target, [`first`, `other`])
    result == false

  @example "with canBeNull"
    let target = { first: 1, second: 5, other: null }
    let result = contains(target, [`first`, `other`], true)
    result == true

  @example "without canBeNull"
    let target = { first: 1, second: 5, other: null }
    let result = contains(target, [`first`, `other`])
    result == false

*/
module.exports = function(target, fields, canBeNull = false){
  for(field of fields){
    if(typeof target[field] == `undefined` || (target[field] == null && !canBeNull)){
      return false
    }
  }
  return true
}

/**

  @description
    adds value to [target] by rules:
      if [target] is object put [value] by [key] name
      if [target] is array push [value]

  @param {object/array}   target - object or array to added value
  @param {string/number}  key - key name of added value
  @param {any}            value - added value
  @return {object/array}

  @example "target is object"
    let target = {}
    add(target, `value`, 12)
    target == { value: 12 }

  @example "target is array"
    let target = []
    add(target, `value`, 12)
    target == [12]

*/

module.exports = function(target, key, value){
  if(Array.isArray(target)) target.push(value)
  else target[key] = value
}

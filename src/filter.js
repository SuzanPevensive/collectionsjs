const add = require(`./add`)

//@browserify:getter
const { empty } = require(`@suzan_pevensive/mock-js`)

/**

  @description
    returns a new [object/array] filled [fields] from [target], which fulfills [condition]

  @param {object/array}   target - source object or array
  @param {function}       condition - condition method testing key and value of current entry
  @return {object/array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = filter(target, (key, value)=>( value > 9 ))
    result == { other: 12 }

*/
module.exports = function(target, condition){
  //@browserify:get(empty)
  let filtered = empty(target)
  for(key in target){
    if(condition(key, target[key])) add(filtered, key, target[key])
  }
  return filtered
}

/**

  @description
    return true if any iteration over [target] entries fulfills [condition]

  @param {object/array}   target - object or array to added value
  @param {function}       condition - condition method testing key and value of current entry
  @return {boolean}

  @example "condition fulfilled"
    let target = { first: 1, second: 5, other: 12 }
    let result = any(target, (key, value)=>( value > 9 ))
    result == true

  @example "condition not fulfilled"
    let target = { first: 1, second: 5 }
    let result = any(target, (key, value)=>( value > 9 ))
    result == false

*/
module.exports = function(target, condition){
  for(key in target){
    if(condition(key, target[key])) return true
  }
  return false
}

const forEach = require(`./forEach`)

/**

  @description
    flattens [object] and return as [array] contains [values] returned by [callback]

  @param {object}     object - object to flatten
  @param {function}   callback - function parsed array values
  @return {array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = flat(target, (key, value)=>( value - 2 ))
    result == [ -1, 3, 10 ]

*/
module.exports = function(object, callback){
  let array = []
  forEach(object, (key, value) => {
    array.push(callback(key, value))
  })
  return array
}

const forEach = require(`./forEach`)
const add = require(`./add`)

//@browserify:getter
const { empty } = require(`@suzan_pevensive/mock-js`)

/**

  @description
    returns a new [object/array] filled specific [fields] from [target]

  @param {object/array}   target - object or array as source
  @param {array}          fields - name of fields to copy from source [target]
  @return {object/array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = cut(target, [`first`, `second`])
    result == { first: 1, second: 5 }

*/
module.exports = function(target, fields){
  //@browserify:get(empty)
  let obj = empty(target)
  forEach(target, (key, value) => {
    if(fields.includes(key)) add(obj, key, value)
  })
  return obj
}

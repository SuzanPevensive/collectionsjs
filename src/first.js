/**

  @description
    returns first [field] from [target], which fulfills [condition]

  @param {object/array}   target - object or array to get value
  @param {function}       condition - condition method testing key and value of current entry
  @return {any}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = first(target, (key, value)=>( value > 2 ))
    result == 5

*/
module.exports = function(target, condition){
  for(key in target){
    if(condition(key, target[key])) return target[key]
  }
  return null
}

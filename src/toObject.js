const forEach = require(`./forEach`)

/**

  @description
    converts [array] to new [object] with indexes as keys

  @param {array}     array - array as source to conversion
  @return {object}

  @example
    let target = [1, 5, 12]
    let result = toObject(target)
    result == { "0": 1, "1": 5, "2": 12 }

*/
module.exports = function(array, callback){
  let object = {}
  forEach(array, (index, value) => {
    object[index] = value
  })
  return object
}

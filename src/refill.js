/**

  @description
    returns a new [array] from source [array] refilled [fields]
      if source array contains value, value don't be duplicated

  @param {array}          array - array as source
  @param {array}          fields - fields to refill
  @return {object/array}

  @example
    let target = [ 1, true, `some text ...`]
    let result = refill(target, [1, true, null, `some other text...`])
    result == [ 1, true, `some text ...`, null, `some other text...`]

*/
module.exports = function(array, fields){
  let arrayCopy = array.slice()
  fields.forEach((field)=>{
    if(!arrayCopy.includes(field)) arrayCopy.push(field)
  })
  return arrayCopy
}

const flat = require(`./flat`)

/**

  @description
    converts [object] to new [array] and ignore the key names

  @param {object}     object - object as source to conversion
  @return {array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = toArray(target)
    result == [1, 5, 12]

*/
module.exports = function(object){
  return flat(object, (key, value) => value)
}

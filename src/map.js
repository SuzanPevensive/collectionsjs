const forEach = require(`./forEach`)
const add = require(`./add`)

//@browserify:getter
const { empty } = require(`@suzan_pevensive/mock-js`)

/**

  @description
    returns a new [object/array] filled [values] returned by [callback]

  @param {object/array}   target - object or array to iterate
  @param {function}       callback - function parsed values of new object or array
  @return {object/array}

  @example
    let target = { first: 1, second: 5, other: 12 }
    let result = map(target, (key, value)=>( value + 5 ))
    result == { first: 6, second: 10, other: 17 }

*/
module.exports = function(target, callback){
  //@browserify:get(empty)
  let obj = empty(target)
  forEach(target, (key, value) => {
    add(obj, key, callback(key, value))
  })
  return obj
}
